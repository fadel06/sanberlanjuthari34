<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PeminjamanController extends Controller
{
    public function index(){
        $data_peminjaman = \App\PeminjamanModel::all();
        // dd($data_peminjaman);
        return view('peminjaman.index', ['data_peminjaman' => $data_peminjaman]);
    }
}
