<?php

Route::namespace('Auth')->group(function(){
    Route::post('register', 'RegisterController');
    Route::post('login', 'LogInController');
    Route::post('logout', 'LogOutController');
    Route::get('user', 'UsersController');
});

?>