<?php

use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'email' => 'mahasiswa@perpus.com',
            'role_id' => 2,
            'name' => 'Mahasiswa',
            'password' => bcrypt('password')
        ]);
        DB::table('users')->insert([
            'email' => 'admin@perpus.com',
            'role_id' => 1,
            'name' => 'Admin',
            'password' => bcrypt('password')
        ]);
    }
}
