<?php

use Illuminate\Database\Seeder;

class BukuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('buku')->insert([
            'kode_buku' =>  'A0001',
            'judul_buku' => 'Anak Periang',
            'pengarang' => 'Bobi',
            'tahun_terbit' => 1945
        ]);
        DB::table('buku')->insert([
            'kode_buku' =>  'B0001',
            'judul_buku' => 'Bijaksana Dalam Berkata',
            'pengarang' => 'Alex',
            'tahun_terbit' => 2000
        ]);
        DB::table('buku')->insert([
            'kode_buku' =>  'C0001',
            'judul_buku' => 'Cerdas Berperilaku',
            'pengarang' => 'Charles',
            'tahun_terbit' => 2001
        ]);
    }
}
