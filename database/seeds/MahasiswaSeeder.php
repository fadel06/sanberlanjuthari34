<?php

use Illuminate\Database\Seeder;

class MahasiswaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('mahasiswa')->insert([
            'nim' =>  'T0200001',
            'nama' => 'Doni',
            'fakultas' => 'Teknik',
            'jurusan' => 'Teknik Tata Boga',
            'no_hp' => '088899990000',
            'no_wa' => '088899990000',
            'user_id' => '1'
        ]);
        DB::table('mahasiswa')->insert([
            'nim' =>  'A0200001',
            'nama' => 'Tere',
            'fakultas' => 'Ekonomi dan Bisnis',
            'jurusan' => 'Akuntansi',
            'no_hp' => '077711112222',
            'no_wa' => '033322228888',
        ]);
        DB::table('mahasiswa')->insert([
            'nim' =>  'K0200001',
            'nama' => 'Roni',
            'fakultas' => 'Sastra',
            'jurusan' => 'Bahasa Hewan',
            'no_hp' => '066633334444',
            'no_wa' => '066633334444'
        ]);
    }
}
