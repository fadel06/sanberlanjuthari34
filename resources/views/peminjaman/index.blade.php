<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{asset('/bootstrap/css/bootstrap.min.css')}}">

    <title>Peminjaman</title>
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-6">
                <h1>Data Peminjaman</h1>
            </div>
            
            <div class="col-6 mt-2">
                <!-- Button trigger modal -->
                <button type="button" class="btn btn-primary float-right mt-2 btn-sm" data-toggle="modal" data-target="#exampleModal">
                    Tambah Data
                </button>
                
                <!-- Modal -->
                <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <form>                            
                                    <div class="form-group">
                                      <label for="Nama">Nama Mahasiswa</label>
                                      <input type="text" class="form-control" id="Nama">
                                    </div>
                                    <div class="form-group">
                                        <label for="Nama Buku">Nama Buku</label>
                                        <input type="text" class="form-control" id="Nama Buku">
                                    </div>
                                    <div class="form-group">
                                        <label for="Tanggal Peminjaman">Tanggal Peminjaman</label>
                                        <input type="date" class="form-control" id="Tanggal Peminjaman">
                                    </div>
    
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </form>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="button" class="btn btn-primary">Save changes</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <table class="table-hover table">
                <tr>
                    <th>NO.</th>
                    <th>NAMA MAHASISWA</th>
                    <th>NAMA BUKU</th>
                    <th>TANGGAL PEMINJAMAN</th>
                    <th>BATAS PEMINJAMAN</th>
                    <th>TANGGAL PENGEMBALIAN</th>
                    <th>STATUS PENGEMBALIAN</th>
                </tr>

                @foreach ($data_peminjaman as $key => $peminjaman)
                <tr>
                    <td>{{$key + 1}}</td>
                    {{-- <td>{{$peminjaman->nim}}</td>
                    <td>{{$peminjaman->nama}}</td>
                    <td>{{$peminjaman->fakultas}}</td>
                    <td>{{$peminjaman->jurusan}}</td>
                    <td>{{$peminjaman->no_hp}}</td>
                    <td>{{$peminjaman->no_wa}}</td> --}}
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>                    
                @endforeach

            </table>
        </div>
    </div>


    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="{{asset('/bootstrap/js/bootstrap.min.js')}}"></script>
</body>
</html>