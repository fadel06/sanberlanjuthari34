<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{asset('/bootstrap/css/bootstrap.min.css')}}">
    
    <title>Mahasiswa</title>
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-6">
                <h1>Data Mahasiswa</h1>
            </div>

            <div class="col-6 mt-2">
                <!-- Button trigger modal -->
                <button type="button" class="btn btn-primary float-right btn-sm" data-toggle="modal" data-target="#exampleModal1   ">
                    Tambah Data Mahasiswa
                </button>
                
                <!-- Modal -->
                <div class="modal fade" id="exampleModal1" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        </div>
                        <div class="modal-body">
                            <form>
                                <div class="form-group">
                                  <label for="Nim">NIM</label>
                                  <input type="text" class="form-control" id="Nim">
                                </div>
                                <div class="form-group">
                                  <label for="Nama">Nama</label>
                                  <input type="text" class="form-control" id="Nama">
                                </div>
                                <div class="form-group">
                                    <label for="Fakultas">Fakultas</label>
                                    <input type="text" class="form-control" id="Fakultas">
                                </div>
                                <div class="form-group">
                                    <label for="Jurusan">Jurusan</label>
                                    <input type="text" class="form-control" id="Jurusan">
                                </div>
                                <div class="form-group">
                                    <label for="Nomor HP">Nomor HP</label>
                                    <input type="number" class="form-control" id="Nomor HP">
                                </div>
                                <div class="form-group">
                                    <label for="Nomor WA">Nomor WA</label>
                                    <input type="number" class="form-control" id="Nomor WA">
                                </div>

                                <button type="submit" class="btn btn-primary">Submit</button>
                            </form>
                        </div>
                        <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary">Save changes</button>
                        </div>
                    </div>
                    </div>
                </div>
            </div>

            <table class="table table-hover">
                <tr>
                    <th>NO.</th>
                    <th>NIM</th>
                    <th>NAMA</th>
                    <th>FAKULTAS</th>
                    <th>JURUSAN</th>
                    <th>NOMOR HP</th>
                    <th>NOMOR WA</th>
                </tr>

                @foreach ($data_mahasiswa as $key => $mahasiswa)
                <tr>
                    <td>{{$key + 1}}</td>
                    <td>{{$mahasiswa->nim}}</td>
                    <td>{{$mahasiswa->nama}}</td>
                    <td>{{$mahasiswa->fakultas}}</td>
                    <td>{{$mahasiswa->jurusan}}</td>
                    <td>{{$mahasiswa->no_hp}}</td>
                    <td>{{$mahasiswa->no_wa}}</td>
                </tr>                    
                @endforeach

            </table>
        </div>
    </div>



    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="{{asset('/bootstrap/js/bootstrap.min.js')}}"></script>
</body>
</html>