<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{asset('/bootstrap/css/bootstrap.min.css')}}">
    
    <title>Buku</title>
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-6">
                <h1>Data Buku</h1>
            </div>

            <div class="col-6 mt-2">
                <button type="button" class="btn btn-primary float-right btn-sm" data-toggle="modal" data-target="#exampleModal">
                    Tambah Data Buku
                </button>

                <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                      
                            <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            </div>
                            <div class="modal-body">
                            ...
                            </div>
                            <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-primary">Save changes</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <table class="table-hover table">
                <tr>
                    <th>NO.</th>
                    <th>KODE BUKU</th>
                    <th>JUDUL</th>
                    <th>PENGARANG</th>
                    <th>TAHUN TERBIT</th>
                </tr>

                @foreach ($data_buku as $key => $buku)
                <tr>
                    <td>{{$key + 1}}</td>
                    <td>{{$buku->kode_buku}}</td>
                    <td>{{$buku->judul_buku}}</td>
                    <td>{{$buku->pengarang}}</td>
                    <td>{{$buku->tahun_terbit}}</td>
                </tr>                    
                @endforeach

            </table>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="{{asset('/bootstrap/js/bootstrap.min.js')}}"></script>
</body>
</html>